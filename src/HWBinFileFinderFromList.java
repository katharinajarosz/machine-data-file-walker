import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class HWBinFileFinderFromList extends SimpleFileVisitor<Path> {


    private final static Logger LOGGER = Logger.getLogger(FileWalker2.class.getName());
    private static Path abortionFile = Paths.get("C:\\\\development", "MDaten.csv");
    private FileHandler fh;
    private static Path hwBin = Paths.get("\\\\", "NAS01", "MachineData", "RohData_LK2001");
    private static final String FILE_PATTERN = "glob:*.hwbin";
    private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    ArrayList<AbortionTime> abortionTimes = new ArrayList<>();
    Set<String> machineNumbers = new LinkedHashSet<>();

    Set<String> abortionFiles = new LinkedHashSet<>();

    public HWBinFileFinderFromList() {
        // TODO Auto-generated constructor stub
        SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
        try {
            fh = new FileHandler("C:/Data/test/MyLogFile_" + format.format(Calendar.getInstance().getTime()) + ".log");
        } catch (Exception e) {
            e.printStackTrace();
        }

        fh.setFormatter(new SimpleFormatter());
        LOGGER.addHandler(fh);
        // LOGGER.setUseParentHandlers(false);
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {

        abortionTimes = readAbortionFile();
        machineNumbers = getDirectories(abortionTimes);
        String directory = dir.getFileName().toString();

        if (directory.startsWith("0") || directory.startsWith("M")) {
            if (!machineNumbers.contains(directory)) {
                return FileVisitResult.SKIP_SUBTREE;
            }
        }
        // LOGGER.info("XXX" + directory);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

        Path name = file.getFileName();

        if (attrs.isRegularFile() && matcher.matches(name)) {
            String[] parent = file.getParent().toString().split("\\\\");
            String directory = parent[parent.length - 1];

            AbortionTime abortionTime = abortionTimes.stream().filter(input -> Objects.equals(input.getMachineNumber(), directory)).findFirst().orElse(null);

            String[] timeArray = name.toString().split("_");
            String dateString = timeArray[2].concat(".").concat(timeArray[1]).concat(".").concat(timeArray[0]);

            LocalDate localDate = LocalDate.parse(dateString, formatter);

            if (Integer.parseInt(timeArray[0]) > 2020) {

                if (machineNumbers.contains(directory)) {
                    ArrayList<LocalDate> localDates = abortionTime.getStartTimes();

                    if (localDates.contains(localDate)) {
                        // LOGGER.info("Parent Directory: " + directory + " START DATE: " + localDateStart + " DAY AFTER " + localDateEnd);
                        DecimalFormat dFormatter = new DecimalFormat("#0.00");
                        long size = attrs.size();
                        double b = (double) size;
                        double tb = b * 1e-12;
                        double mb = b * 1e-6;
                        if (mb > 200) {
                            abortionFiles.add(directory.concat(";")
                                    .concat(localDate.format(formatter))
                                    .concat(";")
                                    .concat(name.toString())
                                    .concat(";")
                                    .concat(dFormatter.format(mb))
                                    .concat(";")
                                    .concat(String.valueOf(size))
                                    .concat("\n"));
                        }
                    }
                }
            }
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return super.visitFileFailed(file, exc);
    }

    private ArrayList<AbortionTime> readAbortionFile() {

        try (BufferedReader bf = Files.newBufferedReader(abortionFile)) {

            String line;
            while ((line = bf.readLine()) != null) {

                String[] split = line.split(";");
                String machineNumber = split[1];
                String endTime = split[0];
                LocalDate localEndDate = LocalDate.parse(endTime, formatter);
                LocalDate localStartDate = localEndDate.minusDays(1);

                AbortionTime abortionTime = abortionTimes.stream().filter(input -> Objects.equals(input.getMachineNumber(), machineNumber)).findFirst().orElse(null);

                if (abortionTime == null) {
                    abortionTime = new AbortionTime();
                    abortionTime.setMachineNumber(machineNumber);
                    ArrayList<LocalDate> startTimes = new ArrayList<>();
                    startTimes.add(localStartDate);
                    startTimes.add(localEndDate);
                    abortionTime.setStartTimes(startTimes);
                    abortionTimes.add(abortionTime);
                } else {
                    abortionTime.getStartTimes().add(localStartDate);
                    abortionTime.getStartTimes().add(localEndDate);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return abortionTimes;
    }

    private Set<String> getDirectories(ArrayList<AbortionTime> abortionTimes) {

        for (AbortionTime abortionTime : abortionTimes) {
            machineNumbers.add(abortionTime.getMachineNumber());
        }
        return machineNumbers;
    }

    public void printResult() throws IOException {
        LOGGER.info(abortionFiles.toString());

        BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\TEMP\\test\\abortionFiles.csv"));

        for (String file: abortionFiles) {
            writer.write(file);
        }
        writer.close();
    }


    public static void main(String[] args) {

        HWBinFileFinderFromList hwBinFileFinder = new HWBinFileFinderFromList();
        try {
            Files.walkFileTree(hwBin, hwBinFileFinder);
            hwBinFileFinder.printResult();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
