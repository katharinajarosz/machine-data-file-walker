import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class FileWalker extends SimpleFileVisitor<Path> {

	private final static Logger LOGGER = Logger.getLogger(FileWalker.class.getName());
	private FileHandler fh;
	private static Path hwBin = Paths.get("\\\\", "NAS01", "MachineData", "RohData_LK2001");
	private static Path hwBin2 = Paths.get("C:\\\\HWBinFiles_klein");
	private static final String FILE_PATTERN = "glob:*.hwbin";
	private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
	String jahr = "2020";
	String nextYear = "2021";
	// First
	int count1 = 0;
	long gesamt1 = 0;
	double tb1 = 0;
	// Second
	FileTime ft2 = FileTime.from(Instant.parse("2019-01-01T00:00:00.00Z"));
	int count2 = 0;
	long gesamt2 = 0;
	double tb2 = 0;
	// Third
	FileTime ft3 = FileTime.from(Instant.parse("2020-01-01T00:00:00.00Z"));
	int count3 = 0;
	long gesamt3 = 0;
	double tb3 = 0;

	// January
	FileTime january = FileTime.from(Instant.parse(jahr + "-01-01T00:00:00.00Z"));
	int januaryCount = 0;
	long januarySize = 0;
	double januaryMB = 0;
	// February
	FileTime february = FileTime.from(Instant.parse(jahr + "-02-01T00:00:00.00Z"));
	int februaryCount = 0;
	long februarySize = 0;
	double februaryMB = 0;
	// March
	FileTime march = FileTime.from(Instant.parse(jahr + "-03-01T00:00:00.00Z"));
	int marchCount = 0;
	long marchSize = 0;
	double marchMB = 0;
	// April
	FileTime april = FileTime.from(Instant.parse(jahr + "-04-01T00:00:00.00Z"));
	int aprilCount = 0;
	long aprilSize = 0;
	double aprilMB = 0;
	// May
	FileTime may = FileTime.from(Instant.parse(jahr + "-05-01T00:00:00.00Z"));
	int mayCount = 0;
	long maySize = 0;
	double mayMB = 0;
	// June
	FileTime june = FileTime.from(Instant.parse(jahr + "-06-01T00:00:00.00Z"));
	int juneCount = 0;
	long juneSize = 0;
	double juneMB = 0;
	// July
	FileTime july = FileTime.from(Instant.parse(jahr + "-07-01T00:00:00.00Z"));
	int julyCount = 0;
	long julySize = 0;
	double julyMB = 0;
	// August
	FileTime august = FileTime.from(Instant.parse(jahr + "-08-01T00:00:00.00Z"));
	int augustCount = 0;
	long augustSize = 0;
	double augustMB = 0;
	// September
	FileTime september = FileTime.from(Instant.parse(jahr + "-09-01T00:00:00.00Z"));
	int septemberCount = 0;
	long septemberSize = 0;
	double septemberMB = 0;
	// October
	FileTime october = FileTime.from(Instant.parse(jahr + "-10-01T00:00:00.00Z"));
	int octoberCount = 0;
	long octoberSize = 0;
	double octoberMB = 0;
	// November
	FileTime november = FileTime.from(Instant.parse(jahr + "-11-01T00:00:00.00Z"));
	int novemberCount = 0;
	long novemberSize = 0;
	double novemberMB = 0;
	// December
	FileTime december = FileTime.from(Instant.parse(jahr + "-12-01T00:00:00.00Z"));
	int decemberCount = 0;
	long decemberSize = 0;
	double decemberMB = 0;
	// Next January
	FileTime nextJan = FileTime.from(Instant.parse(nextYear + "-01-01T00:00:00.00Z"));

	public FileWalker() {
		// TODO Auto-generated constructor stub
		SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
		try {
			fh = new FileHandler("C:/temp/test/MyLogFile_" + format.format(Calendar.getInstance().getTime()) + ".log");
		} catch (Exception e) {
			e.printStackTrace();
		}

		fh.setFormatter(new SimpleFormatter());
		LOGGER.addHandler(fh);
		// LOGGER.setUseParentHandlers(false);
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

		Path name = file.getFileName();
		if (attrs.isRegularFile() && matcher.matches(name)) {
			long size = attrs.size();
			double b = (double) size;
			double tb = b * 1e-12;
			double mb = b * 1e-9;

			gesamt1 = gesamt1 + size;
			tb1 = tb1 + tb;
			count1++;

			FileTime created = attrs.lastModifiedTime();

			int comp2 = created.compareTo(ft2);
			if (comp2 > 0) {
				gesamt2 = gesamt2 + size;
				tb2 = tb2 + tb;
				count2++;
			}

			int comp3 = created.compareTo(ft3);
			if (comp3 > 0) {
				gesamt3 = gesamt3 + size;
				tb3 = tb3 + tb;
				count3++;
			}

			
			int m1 = created.compareTo(january);
			int m2 = created.compareTo(february);
			int m3 = created.compareTo(march);
			int m4 = created.compareTo(april);
			int m5 = created.compareTo(may);
			int m6 = created.compareTo(june);
			int m7 = created.compareTo(july);
			int m8 = created.compareTo(august);
			int m9 = created.compareTo(september);
			int m10 = created.compareTo(october);
			int m11 = created.compareTo(november);
			int m12 = created.compareTo(december);
			int m01 = created.compareTo(nextJan);

			

			if (m1 >= 0 && m2 < 0) {
				januarySize = januarySize + size;
				januaryMB = januaryMB + mb;
				januaryCount++;
			}

			if (m2 >= 0 && m3 < 0) {
				februarySize = februarySize + size;
				februaryMB = februaryMB + mb;
				februaryCount++;
			}

			if (m3 >= 0 && m4 < 0) {
				marchSize = marchSize + size;
				marchMB = marchMB + mb;
				marchCount++;
			}

			if (m4 >= 0 && m5 < 0) {
				aprilSize = aprilSize + size;
				aprilMB = aprilMB + mb;
				aprilCount++;
			}
			
			if (m5 >= 0 && m6 < 0) {
				maySize = maySize + size;
				mayMB = mayMB + mb;
				mayCount++;
			}
			
			if (m6 >= 0 && m7 < 0) {
				juneSize = juneSize + size;
				juneMB = juneMB + mb;
				juneCount++;
			}
			
			if (m7 >= 0 && m8 < 0) {
				julySize = julySize + size;
				julyMB = julyMB + mb;
				julyCount++;
			}			
			
			if (m8 >= 0 && m9 < 0) {
				augustSize = augustSize + size;
				augustMB = augustMB + mb;
				augustCount++;
			}

			if (m9 >= 0 && m10 < 0) {
				septemberSize = septemberSize + size;
				septemberMB = septemberMB + mb;
				septemberCount++;
			}

			if (m10 >= 0 && m11 < 0) {
				octoberSize = octoberSize + size;
				octoberMB = octoberMB + mb;
				octoberCount++;
			}

			if (m11 >= 0 && m12 < 0) {
				novemberSize = novemberSize + size;
				novemberMB = novemberMB + mb;
				novemberCount++;
			}

			if (m12 >= 0 && m01 < 0) {
				decemberSize = decemberSize + size;
				decemberMB = decemberMB + mb;
				decemberCount++;
			}

		}

		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

		return super.visitFileFailed(file, exc);
	}

	public void printResult() {

		DecimalFormat formatter = new DecimalFormat("#0.00");
		LOGGER.info("Path: " + hwBin + " | Size gesamt: " + gesamt1 + " bytes/ " + formatter.format(tb1) + " TB "
				+ " | Anzahl Files: " + count1);

		LOGGER.info("Path: " + hwBin + " | Size ab dem " + ft2 + " : " + gesamt2 + " bytes/ " + formatter.format(tb2)
				+ " TB " + " | Anzahl Files: " + count2);

		LOGGER.info("Path: " + hwBin + " | Size ab dem " + ft3 + " : " + gesamt3 + " bytes/ " + formatter.format(tb3)
				+ " TB " + " | Anzahl Files: " + count3);

		LOGGER.info("---------------------------- Jahr " + jahr + " ----------------------------");
		
		LOGGER.info("Size Januar: " + formatter.format(januaryMB) + " GB | Anzahl: " + januaryCount);
		LOGGER.info("Size Februar: " + formatter.format(februaryMB) + " GB | Anzahl: " + februaryCount);
		LOGGER.info("Size M�rz: " + formatter.format(marchMB) + " GB | Anzahl: " + marchCount);
		LOGGER.info("Size April: " + formatter.format(aprilMB) + " GB | Anzahl: " + aprilCount);
		LOGGER.info("Size Mai: " + formatter.format(mayMB) + " GB | Anzahl: " + mayCount);
		LOGGER.info("Size Juni: " + formatter.format(juneMB) + " GB | Anzahl: " + juneCount);
		LOGGER.info("Size Juli: " + formatter.format(julyMB) + " GB | Anzahl: " + julyCount);
		LOGGER.info("Size August: " + formatter.format(augustMB) + " GB | Anzahl: " + augustCount);
		LOGGER.info("Size Sepetmber: " + formatter.format(septemberMB) + " GB | Anzahl: " + septemberCount);
		LOGGER.info("Size Oktober: " + formatter.format(octoberMB) + " GB | Anzahl: " + octoberCount);
		LOGGER.info("Size November: " + formatter.format(novemberMB) + " GB | Anzahl: " + novemberCount);
		LOGGER.info("Size Dezember: " + formatter.format(decemberMB) + " GB | Anzahl: " + decemberCount);
		LOGGER.info("ENDE");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			FileWalker hwBinFileFinder = new FileWalker();
			LOGGER.info("START");
			Files.walkFileTree(hwBin, hwBinFileFinder);
			hwBinFileFinder.printResult();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
