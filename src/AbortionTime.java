import java.time.LocalDate;
import java.util.ArrayList;

public class AbortionTime {

    private String machineNumber;
    private ArrayList<LocalDate> startTimes;

    public String getMachineNumber() {
        return machineNumber;
    }

    public void setMachineNumber(String machineNumber) {
        this.machineNumber = machineNumber;
    }

    public ArrayList<LocalDate> getStartTimes() {
        return startTimes;
    }

    public void setStartTimes(ArrayList<LocalDate> startTimes) {
        this.startTimes = startTimes;
    }
}
