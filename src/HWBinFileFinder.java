import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class HWBinFileFinder extends SimpleFileVisitor<Path> {

    private final static Logger LOGGER = Logger.getLogger(HWBinFileFinder.class.getName());
    private FileHandler fh;

    private final Path filePath = Paths.get("\\\\", "NAS01", "MachineData", "RohData_LK2001");

    //    FileTime backwardDays = FileTime.from(Instant.now().minus(10, ChronoUnit.DAYS));
    FileTime backwardDays = FileTime.from(Instant.parse("2021-11-01T00:00:00.00Z"));
    private static final String FILE_PATTERN = "glob:*.hwbin";
    private final PathMatcher matcher;
    private final Queue<Path> pathQueue;

    long gesamt1 = 0;
    double mb1 = 0;

    public HWBinFileFinder() {
        SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
        try {
            fh = new FileHandler("C:/Data/test/MyLogFile_" + format.format(Calendar.getInstance().getTime()) + ".log");
        } catch (Exception e) {
            e.printStackTrace();
        }

        fh.setFormatter(new SimpleFormatter());
        LOGGER.addHandler(fh);
        this.pathQueue = new ConcurrentLinkedQueue<>();
        matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
    }

    @Override
    public FileVisitResult preVisitDirectory(Path file, BasicFileAttributes attrs) {

        String name = file.getFileName().toString();

        return switch (name) {
            case "MP1", "MP2", "MP3", "MP4" -> FileVisitResult.SKIP_SUBTREE;
            default -> FileVisitResult.CONTINUE;
        };

    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {

        if (attrs.isRegularFile() && matcher.matches(file.getFileName())) {

            FileTime created = attrs.creationTime();
            FileTime modified = attrs.lastModifiedTime();
            int c1 = created.compareTo(backwardDays);
            int c2 = modified.compareTo(backwardDays);
            if (c2 > 0) {
                long size = attrs.size();
                double b = (double) size;
                double mb = b * 1e-9;
                gesamt1 = gesamt1 + size;
                mb1 = mb1 + mb;
//                LOGGER.info(file.getFileName().toString());
                pathQueue.add(file);
            }

        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

        return super.visitFileFailed(file, exc);
    }

    public void getPathQueue() throws IOException {
        Files.walkFileTree(filePath, this);
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        try {
            HWBinFileFinder hwBinFileFinder = new HWBinFileFinder();
//            Files.walkFileTree(hwBinFileFinder.filePath, hwBinFileFinder);
            LOGGER.info("Backward Days: " + hwBinFileFinder.backwardDays);
            Instant start = Instant.now();
            hwBinFileFinder.getPathQueue();
            LOGGER.info("Path Queue Size: " + hwBinFileFinder.pathQueue.size());
            Instant end = Instant.now();
            Duration dif = Duration.between(start, end);
            LOGGER.info("Duration: " + dif.getSeconds() + " S");
            DecimalFormat formatter = new DecimalFormat("#0.00");
            LOGGER.info("Files Size: " + hwBinFileFinder.gesamt1 + " | " + formatter.format(hwBinFileFinder.mb1) + " MB");

        } catch (IOException e) {

            e.printStackTrace();
        }

    }
}