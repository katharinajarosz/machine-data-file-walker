import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class FileWalker2 extends SimpleFileVisitor<Path> {

	private final static Logger LOGGER = Logger.getLogger(FileWalker2.class.getName());
	private FileHandler fh;
	// private static Path hwBin2 = Paths.get("\\\\", "NAS01", "MachineData", "RohData_LK2001");
	private static Path hwBin = Paths.get("C:\\HwBinFiles");
	private static final String UE_LOG_FILE_PATTERN = "glob:*.uelog";
	private final PathMatcher ueLogMatcher = FileSystems.getDefault().getPathMatcher(UE_LOG_FILE_PATTERN);
	private Queue<Path> pathQueue = new ConcurrentLinkedQueue<>();;
	
	
	public FileWalker2() {
		// TODO Auto-generated constructor stub
		SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
		try {
			fh = new FileHandler("C:/Data/test/MyLogFile_" + format.format(Calendar.getInstance().getTime()) + ".log");
		} catch (Exception e) {
			e.printStackTrace();
		}

		fh.setFormatter(new SimpleFormatter());
		LOGGER.addHandler(fh);
		// LOGGER.setUseParentHandlers(false);
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

		Path name = file.getFileName();
		if (attrs.isRegularFile() && ueLogMatcher.matches(name)) {
			long size = attrs.size();
			double b = (double) size;
			double tb = b * 1e-12;
			double mb = b * 1e-9;

			pathQueue.add(file);
						
		}

		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

		return super.visitFileFailed(file, exc);
	}
	
	public boolean readAllLines() {
		
		String content = "";
		
		if (pathQueue.peek() != null) {
			Path element = pathQueue.poll();
			
			try (BufferedReader bf = Files.newBufferedReader(element)) {
				
				String fileName = element.getFileName().toString();
				
				LOGGER.info("-------------------------------------------------------------------------------" + fileName);
				
				String line;
				while ((line = bf.readLine()) != null) {
					String[] a = line.split(" ");
					if (a[3].equalsIgnoreCase("Error") && a[4].equalsIgnoreCase("Quit")) {
						String[] date = a[1].split("\\.");
						String[] time = a[0].split(":");
						String[] sec = time[2].split(",");
						String d = date[0];
						String m = date[1];
						String y = date[2];
						String hh = time[0];
						String mm = time[1];
						String ss = sec[0];
						String ff = sec[1];
						String datetime = y+"-"+m+"-"+d+" "+hh+":"+mm+":"+ss+"."+ff;
						Timestamp timestamp = Timestamp.valueOf(datetime);
						LOGGER.info(a[0] +" " + a[1] + " " + a[3] + " " + a[4] + " " + time[2] + " " + sec[0] + "\n" + datetime + "\n" + timestamp);
					}
					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			FileWalker2 hwBinFileFinder = new FileWalker2();
			LOGGER.info("START");
			Files.walkFileTree(hwBin, hwBinFileFinder);
			while (hwBinFileFinder.readAllLines()) {
				
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
